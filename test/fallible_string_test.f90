module fallible_string_test
    use erloff, only: error_list_t, module_t
    use iso_varying_string, only: var_str
    use json_assertion, only: assert_equals
    use rojff, only:  &
            fallible_json_string_t, &
            fallible_json_value_t, &
            json_integer_t, &
            json_string_unsafe, &
            parse_json_from_string
    use veggies, only: &
            result_t, test_item_t, assert_equals, assert_that, describe, fail, it

    implicit none
    private
    public :: test_fallible_string
contains
    function test_fallible_string() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "constructing a json string", &
                [ it("produces the proper string if its valid", check_valid) &
                , it("produces an error if the string is invalid", check_invalid) &
                , it("passes through an error from a failed fallible value", check_pass_through) &
                , it("catches an incorrect type in a fallible value", check_wrong_type) &
                ])
    end function

    function check_valid() result(result_)
        type(result_t) :: result_

        character(len=*), parameter :: valid_string = "Hello"
        type(fallible_json_string_t) :: maybe_from_character
        type(fallible_json_string_t) :: maybe_from_string
        type(fallible_json_string_t) :: maybe_from_fallible

        maybe_from_character = fallible_json_string_t(valid_string)
        maybe_from_string = fallible_json_string_t(var_str(valid_string))
        maybe_from_fallible = fallible_json_string_t(fallible_json_value_t(maybe_from_character))
        if (any( &
                [ maybe_from_character%failed() &
                , maybe_from_string%failed() &
                , maybe_from_fallible%failed() &
                ])) then
            block
                type(error_list_t) :: errors

                errors = error_list_t( &
                    [ maybe_from_character%errors &
                    , maybe_from_string%errors &
                    , maybe_from_fallible%errors])
                result_ = fail(errors%to_string())
            end block
        else
            result_ = &
                assert_equals(json_string_unsafe(valid_string), maybe_from_character%string) &
                .and.assert_equals(json_string_unsafe(valid_string), maybe_from_string%string) &
                .and.assert_equals(json_string_unsafe(valid_string), maybe_from_fallible%string)
        end if
    end function

    function check_invalid() result(result_)
        type(result_t) :: result_

        character(len=*), parameter :: invalid_string = 'thi"ng'
        type(fallible_json_string_t) :: maybe_from_character
        type(fallible_json_string_t) :: maybe_from_string

        maybe_from_character = fallible_json_string_t(invalid_string)
        maybe_from_string = fallible_json_string_t(var_str(invalid_string))
        result_ = &
            assert_that( &
                maybe_from_character%errors.hasAnyFrom.module_t("rojff_fallible_json_string_m"), &
                maybe_from_character%errors%to_string()) &
            .and.assert_that( &
                maybe_from_string%errors.hasAnyFrom.module_t("rojff_fallible_json_string_m"), &
                maybe_from_string%errors%to_string())
    end function

    function check_pass_through() result(result_)
        type(result_t) :: result_

        type(fallible_json_value_t) :: failed_json
        type(fallible_json_string_t) :: failed_string

        failed_json = parse_json_from_string("invalid")
        failed_string = fallible_json_string_t(failed_json)
        result_ = assert_equals( &
                failed_json%errors%to_string(), failed_string%errors%to_string())
    end function

    function check_wrong_type() result(result_)
        type(result_t) :: result_

        type(fallible_json_string_t) :: maybe_string

        maybe_string = fallible_json_string_t(fallible_json_value_t(json_integer_t(42)))
        result_ = assert_that( &
                maybe_string%errors.hasAnyFrom.module_t("rojff_fallible_json_string_m"), &
                maybe_string%errors%to_string())
    end function
end module